//
//  ViewController3.swift
//  la_roulette
//
//  Created by etudiant on 10/04/2019.
//  Copyright © 2019 etudiant. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {
    @IBOutlet weak var addmoney: UITextField!
    @IBOutlet weak var setmoney: UIButton!
    @IBOutlet weak var affichebank: UILabel!
    static var bank = 0
    @IBOutlet weak var moneyadded: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        affichebank.text = String(ViewController3.bank) + "$"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //ajoute de l'argent à la banque
    @IBAction func addMoney(_ sender: Any) {
        if(addmoney.text != ""){
            ViewController3.bank = ViewController3.bank + Int(addmoney.text!)!
            affichebank.text = String(ViewController3.bank)+"$"
            moneyadded.text = "vous avez ajouté " + addmoney.text! + "$"
        }

    }

    //maintient l'orientation de l'application en mode portrait
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
