//
//  ViewController2.swift
//  la_roulette
//
//  Created by etudiant on 09/04/2019.
//  Copyright © 2019 etudiant. All rights reserved.
//

import UIKit

class ViewController2: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var moneyfield: UITextField!
    @IBOutlet weak var button_bet: UIButton!
    @IBOutlet weak var bankaffiche: UILabel!
    @IBOutlet weak var moneystatus: UILabel!


    let colors = ["28 black ", "9 red", "26 black", " 30 red", "11 black",
                  "7 red", "20 black", "32 red", "17 black", "5 red", "22 black", "34 red", "15 black", "3 red",
                  "24 black", "36 red", "13 black", "1 red", "00", "27 red", "10 black", "25 red", "29 black",
                  "12 red", "8 black", "19 red", "31 black", "18 red", "9 black", "21 red", "33 black", "16 red","4 black", "23 red", "35 black", "14 red", "2 red", "0"]
    static var chosencolor:String = " "
    static var money = 0
    static var placedbet = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        bankaffiche.text = String(ViewController3.bank)+"$"

        // Do any additional setup after loading the view.
    pickerView.delegate = self
    pickerView.dataSource = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }


    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count
    }
    //choix de la couleur et du chiffre sur lequel on veut placer un paris
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        ViewController2.chosencolor = colors[row]
        return colors[row]
    }


    //Placement du paris

    @IBAction func placebet(_ sender: Any) {
        if(moneyfield.text != ""){
            if(ViewController3.bank >= Int(moneyfield.text!)!){
                ViewController3.bank = ViewController3.bank - Int(moneyfield.text!)!
                bankaffiche.text = String(ViewController3.bank) + "$"
                ViewController2.placedbet = ViewController2.placedbet + Int(moneyfield.text!)!
                moneystatus.text = ""
            }
            else{
                moneystatus.text = "You are broke, go home"

            }
        }
    }

    //maintient l'orientation en mode portrait

    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
