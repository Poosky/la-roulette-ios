//
//  ViewController4.swift
//  la_roulette
//
//  Created by etudiant on 10/04/2019.
//  Copyright © 2019 etudiant. All rights reserved.
//

import UIKit

class ViewController4: UIViewController {
    @IBOutlet weak var affichebank: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var imageview: UIImageView!

    @IBOutlet weak var spin: UIButton!
    @IBOutlet weak var statuslabel: UILabel!







    let numero = ["28 black ", "9 red", "26 black", " 30 red", "11 black",
                  "7 red", "20 black", "32 red", "17 black", "5 red", "22 black", "34 red", "15 black", "3 red",
                  "24 black", "36 red", "13 black", "1 red", "00", "27 red", "10 black", "25 red", "29 black",
                  "12 red", "8 black", "19 red", "31 black", "18 red", "9 black", "21 red", "33 black", "16 red","4 black", "23 red", "35 black", "14 red", "2 red", "0"]

    let rotation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")


    var pickerData: [String] = [String]()

    //fonction qui s'execute à la fin du timer

    @objc func fireTimer() {

        imageview.layer.removeAllAnimations()
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        affichebank.text = String(ViewController3.bank)+"$"
        // Do any additional setup after loading the view.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //fonction de rotation pour l'imageview
    @IBAction func Rotate(_ sender: Any) {
        rotation.toValue = Double.pi * 2
        rotation.duration = 0.5
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        imageview.layer.add(rotation, forKey: "rotationAnimation")


        //timer qui permet l'arrét de la rotation de l'image


        _ = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: false)



        //Affichage du resultat de la partie
        
        let random = numero[Int(arc4random_uniform(UInt32(numero.count)))]
        label1.text = random
        if(random == ViewController2.chosencolor){
            ViewController3.bank = ViewController3.bank + ViewController2.placedbet*36
            statuslabel.text = "You win " + String(ViewController2.placedbet*36) + "$"
            ViewController2.placedbet = 0
            affichebank.text = String(ViewController3.bank) + "$"
        }
        else{
            statuslabel.text = "you loose " + String(ViewController2.placedbet) + "$"
            ViewController2.placedbet = 0
        }







    }

    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
